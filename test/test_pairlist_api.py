"""
    Freqtrade API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 2021.9-dev10
    Generated by: https://openapi-generator.tech
"""


import unittest

import ftclient
from ftclient.api.pairlist_api import PairlistApi  # noqa: E501


class TestPairlistApi(unittest.TestCase):
    """PairlistApi unit test stubs"""

    def setUp(self):
        self.api = PairlistApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_blacklist_api_v1_blacklist_get(self):
        """Test case for blacklist_api_v1_blacklist_get

        Blacklist  # noqa: E501
        """
        pass

    def test_blacklist_post_api_v1_blacklist_post(self):
        """Test case for blacklist_post_api_v1_blacklist_post

        Blacklist Post  # noqa: E501
        """
        pass

    def test_whitelist_api_v1_whitelist_get(self):
        """Test case for whitelist_api_v1_whitelist_get

        Whitelist  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
