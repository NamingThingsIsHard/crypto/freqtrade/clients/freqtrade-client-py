"""
    Freqtrade API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 2021.9-dev10
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import ftclient
from ftclient.model.strategy_response import StrategyResponse


class TestStrategyResponse(unittest.TestCase):
    """StrategyResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testStrategyResponse(self):
        """Test StrategyResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = StrategyResponse()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
