"""
    Freqtrade API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 2021.9-dev10
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import ftclient
from ftclient.model.backtest_request import BacktestRequest


class TestBacktestRequest(unittest.TestCase):
    """BacktestRequest unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testBacktestRequest(self):
        """Test BacktestRequest"""
        # FIXME: construct object with mandatory attributes with example values
        # model = BacktestRequest()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
