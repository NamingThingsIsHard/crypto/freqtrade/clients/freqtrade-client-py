"""
    Freqtrade API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 2021.9-dev10
    Generated by: https://openapi-generator.tech
"""


import unittest

import ftclient
from ftclient.api.info_api import InfoApi  # noqa: E501


class TestInfoApi(unittest.TestCase):
    """InfoApi unit test stubs"""

    def setUp(self):
        self.api = InfoApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_balance_api_v1_balance_get(self):
        """Test case for balance_api_v1_balance_get

        Balance  # noqa: E501
        """
        pass

    def test_blacklist_api_v1_blacklist_get(self):
        """Test case for blacklist_api_v1_blacklist_get

        Blacklist  # noqa: E501
        """
        pass

    def test_blacklist_post_api_v1_blacklist_post(self):
        """Test case for blacklist_post_api_v1_blacklist_post

        Blacklist Post  # noqa: E501
        """
        pass

    def test_count_api_v1_count_get(self):
        """Test case for count_api_v1_count_get

        Count  # noqa: E501
        """
        pass

    def test_daily_api_v1_daily_get(self):
        """Test case for daily_api_v1_daily_get

        Daily  # noqa: E501
        """
        pass

    def test_delete_lock_api_v1_locks_lockid_delete(self):
        """Test case for delete_lock_api_v1_locks_lockid_delete

        Delete Lock  # noqa: E501
        """
        pass

    def test_delete_lock_pair_api_v1_locks_delete_post(self):
        """Test case for delete_lock_pair_api_v1_locks_delete_post

        Delete Lock Pair  # noqa: E501
        """
        pass

    def test_edge_api_v1_edge_get(self):
        """Test case for edge_api_v1_edge_get

        Edge  # noqa: E501
        """
        pass

    def test_locks_api_v1_locks_get(self):
        """Test case for locks_api_v1_locks_get

        Locks  # noqa: E501
        """
        pass

    def test_logs_api_v1_logs_get(self):
        """Test case for logs_api_v1_logs_get

        Logs  # noqa: E501
        """
        pass

    def test_performance_api_v1_performance_get(self):
        """Test case for performance_api_v1_performance_get

        Performance  # noqa: E501
        """
        pass

    def test_profit_api_v1_profit_get(self):
        """Test case for profit_api_v1_profit_get

        Profit  # noqa: E501
        """
        pass

    def test_show_config_api_v1_show_config_get(self):
        """Test case for show_config_api_v1_show_config_get

        Show Config  # noqa: E501
        """
        pass

    def test_stats_api_v1_stats_get(self):
        """Test case for stats_api_v1_stats_get

        Stats  # noqa: E501
        """
        pass

    def test_status_api_v1_status_get(self):
        """Test case for status_api_v1_status_get

        Status  # noqa: E501
        """
        pass

    def test_trade_api_v1_trade_tradeid_get(self):
        """Test case for trade_api_v1_trade_tradeid_get

        Trade  # noqa: E501
        """
        pass

    def test_trades_api_v1_trades_get(self):
        """Test case for trades_api_v1_trades_get

        Trades  # noqa: E501
        """
        pass

    def test_trades_delete_api_v1_trades_tradeid_delete(self):
        """Test case for trades_delete_api_v1_trades_tradeid_delete

        Trades Delete  # noqa: E501
        """
        pass

    def test_version_api_v1_version_get(self):
        """Test case for version_api_v1_version_get

        Version  # noqa: E501
        """
        pass

    def test_whitelist_api_v1_whitelist_get(self):
        """Test case for whitelist_api_v1_whitelist_get

        Whitelist  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
