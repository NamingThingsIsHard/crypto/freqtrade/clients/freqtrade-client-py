"""
    Freqtrade API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 2021.9-dev10
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import ftclient
from ftclient.model.validation_error import ValidationError
globals()['ValidationError'] = ValidationError
from ftclient.model.http_validation_error import HTTPValidationError


class TestHTTPValidationError(unittest.TestCase):
    """HTTPValidationError unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testHTTPValidationError(self):
        """Test HTTPValidationError"""
        # FIXME: construct object with mandatory attributes with example values
        # model = HTTPValidationError()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
