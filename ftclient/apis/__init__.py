
# flake8: noqa

# Import all APIs into this package.
# If you have many APIs here with many many models used in each API this may
# raise a `RecursionError`.
# In order to avoid this, import only the API that you directly need like:
#
#   from .api.auth_api import AuthApi
#
# or import this package, but before doing it, use:
#
#   import sys
#   sys.setrecursionlimit(n)

# Import APIs into API package:
from ftclient.api.auth_api import AuthApi
from ftclient.api.backtest_api import BacktestApi
from ftclient.api.botcontrol_api import BotcontrolApi
from ftclient.api.candle_data_api import CandleDataApi
from ftclient.api.default_api import DefaultApi
from ftclient.api.info_api import InfoApi
from ftclient.api.locks_api import LocksApi
from ftclient.api.pairlist_api import PairlistApi
from ftclient.api.strategy_api import StrategyApi
from ftclient.api.trading_api import TradingApi
from ftclient.api.webserver_api import WebserverApi
