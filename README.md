# freqtrade-client-py
No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

This Python package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 2021.12-dev21
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.PythonClientCodegen

## Requirements.

Python >= 3.6

## Installation & Usage
### pip install

If the python package is hosted on a repository, you can install directly using:

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import ftclient
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import ftclient
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python

import time
import ftclient
from pprint import pprint
from ftclient.api import auth_api
from ftclient.model.access_and_refresh_token import AccessAndRefreshToken
from ftclient.model.access_token import AccessToken
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)


# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)
    
    try:
        # Token Login
        api_response = api_instance.token_login_api_v1_token_login_post()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling AuthApi->token_login_api_v1_token_login_post: %s\n" % e)
```

## Documentation for API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AuthApi* | [**token_login_api_v1_token_login_post**](docs/AuthApi.md#token_login_api_v1_token_login_post) | **POST** /api/v1/token/login | Token Login
*AuthApi* | [**token_refresh_api_v1_token_refresh_post**](docs/AuthApi.md#token_refresh_api_v1_token_refresh_post) | **POST** /api/v1/token/refresh | Token Refresh
*BacktestApi* | [**api_backtest_abort_api_v1_backtest_abort_get**](docs/BacktestApi.md#api_backtest_abort_api_v1_backtest_abort_get) | **GET** /api/v1/backtest/abort | Api Backtest Abort
*BacktestApi* | [**api_delete_backtest_api_v1_backtest_delete**](docs/BacktestApi.md#api_delete_backtest_api_v1_backtest_delete) | **DELETE** /api/v1/backtest | Api Delete Backtest
*BacktestApi* | [**api_get_backtest_api_v1_backtest_get**](docs/BacktestApi.md#api_get_backtest_api_v1_backtest_get) | **GET** /api/v1/backtest | Api Get Backtest
*BacktestApi* | [**api_start_backtest_api_v1_backtest_post**](docs/BacktestApi.md#api_start_backtest_api_v1_backtest_post) | **POST** /api/v1/backtest | Api Start Backtest
*BotcontrolApi* | [**reload_config_api_v1_reload_config_post**](docs/BotcontrolApi.md#reload_config_api_v1_reload_config_post) | **POST** /api/v1/reload_config | Reload Config
*BotcontrolApi* | [**start_api_v1_start_post**](docs/BotcontrolApi.md#start_api_v1_start_post) | **POST** /api/v1/start | Start
*BotcontrolApi* | [**stop_api_v1_stop_post**](docs/BotcontrolApi.md#stop_api_v1_stop_post) | **POST** /api/v1/stop | Stop
*BotcontrolApi* | [**stop_buy_api_v1_stopbuy_post**](docs/BotcontrolApi.md#stop_buy_api_v1_stopbuy_post) | **POST** /api/v1/stopbuy | Stop Buy
*CandleDataApi* | [**list_available_pairs_api_v1_available_pairs_get**](docs/CandleDataApi.md#list_available_pairs_api_v1_available_pairs_get) | **GET** /api/v1/available_pairs | List Available Pairs
*CandleDataApi* | [**pair_candles_api_v1_pair_candles_get**](docs/CandleDataApi.md#pair_candles_api_v1_pair_candles_get) | **GET** /api/v1/pair_candles | Pair Candles
*CandleDataApi* | [**pair_history_api_v1_pair_history_get**](docs/CandleDataApi.md#pair_history_api_v1_pair_history_get) | **GET** /api/v1/pair_history | Pair History
*CandleDataApi* | [**plot_config_api_v1_plot_config_get**](docs/CandleDataApi.md#plot_config_api_v1_plot_config_get) | **GET** /api/v1/plot_config | Plot Config
*DefaultApi* | [**ping_api_v1_ping_get**](docs/DefaultApi.md#ping_api_v1_ping_get) | **GET** /api/v1/ping | Ping
*InfoApi* | [**balance_api_v1_balance_get**](docs/InfoApi.md#balance_api_v1_balance_get) | **GET** /api/v1/balance | Balance
*InfoApi* | [**blacklist_api_v1_blacklist_get**](docs/InfoApi.md#blacklist_api_v1_blacklist_get) | **GET** /api/v1/blacklist | Blacklist
*InfoApi* | [**blacklist_delete_api_v1_blacklist_delete**](docs/InfoApi.md#blacklist_delete_api_v1_blacklist_delete) | **DELETE** /api/v1/blacklist | Blacklist Delete
*InfoApi* | [**blacklist_post_api_v1_blacklist_post**](docs/InfoApi.md#blacklist_post_api_v1_blacklist_post) | **POST** /api/v1/blacklist | Blacklist Post
*InfoApi* | [**count_api_v1_count_get**](docs/InfoApi.md#count_api_v1_count_get) | **GET** /api/v1/count | Count
*InfoApi* | [**daily_api_v1_daily_get**](docs/InfoApi.md#daily_api_v1_daily_get) | **GET** /api/v1/daily | Daily
*InfoApi* | [**delete_lock_api_v1_locks_lockid_delete**](docs/InfoApi.md#delete_lock_api_v1_locks_lockid_delete) | **DELETE** /api/v1/locks/{lockid} | Delete Lock
*InfoApi* | [**delete_lock_pair_api_v1_locks_delete_post**](docs/InfoApi.md#delete_lock_pair_api_v1_locks_delete_post) | **POST** /api/v1/locks/delete | Delete Lock Pair
*InfoApi* | [**edge_api_v1_edge_get**](docs/InfoApi.md#edge_api_v1_edge_get) | **GET** /api/v1/edge | Edge
*InfoApi* | [**locks_api_v1_locks_get**](docs/InfoApi.md#locks_api_v1_locks_get) | **GET** /api/v1/locks | Locks
*InfoApi* | [**logs_api_v1_logs_get**](docs/InfoApi.md#logs_api_v1_logs_get) | **GET** /api/v1/logs | Logs
*InfoApi* | [**performance_api_v1_performance_get**](docs/InfoApi.md#performance_api_v1_performance_get) | **GET** /api/v1/performance | Performance
*InfoApi* | [**profit_api_v1_profit_get**](docs/InfoApi.md#profit_api_v1_profit_get) | **GET** /api/v1/profit | Profit
*InfoApi* | [**show_config_api_v1_show_config_get**](docs/InfoApi.md#show_config_api_v1_show_config_get) | **GET** /api/v1/show_config | Show Config
*InfoApi* | [**stats_api_v1_stats_get**](docs/InfoApi.md#stats_api_v1_stats_get) | **GET** /api/v1/stats | Stats
*InfoApi* | [**status_api_v1_status_get**](docs/InfoApi.md#status_api_v1_status_get) | **GET** /api/v1/status | Status
*InfoApi* | [**sysinfo_api_v1_sysinfo_get**](docs/InfoApi.md#sysinfo_api_v1_sysinfo_get) | **GET** /api/v1/sysinfo | Sysinfo
*InfoApi* | [**trade_api_v1_trade_tradeid_get**](docs/InfoApi.md#trade_api_v1_trade_tradeid_get) | **GET** /api/v1/trade/{tradeid} | Trade
*InfoApi* | [**trades_api_v1_trades_get**](docs/InfoApi.md#trades_api_v1_trades_get) | **GET** /api/v1/trades | Trades
*InfoApi* | [**trades_delete_api_v1_trades_tradeid_delete**](docs/InfoApi.md#trades_delete_api_v1_trades_tradeid_delete) | **DELETE** /api/v1/trades/{tradeid} | Trades Delete
*InfoApi* | [**version_api_v1_version_get**](docs/InfoApi.md#version_api_v1_version_get) | **GET** /api/v1/version | Version
*InfoApi* | [**whitelist_api_v1_whitelist_get**](docs/InfoApi.md#whitelist_api_v1_whitelist_get) | **GET** /api/v1/whitelist | Whitelist
*LocksApi* | [**delete_lock_api_v1_locks_lockid_delete**](docs/LocksApi.md#delete_lock_api_v1_locks_lockid_delete) | **DELETE** /api/v1/locks/{lockid} | Delete Lock
*LocksApi* | [**delete_lock_pair_api_v1_locks_delete_post**](docs/LocksApi.md#delete_lock_pair_api_v1_locks_delete_post) | **POST** /api/v1/locks/delete | Delete Lock Pair
*LocksApi* | [**locks_api_v1_locks_get**](docs/LocksApi.md#locks_api_v1_locks_get) | **GET** /api/v1/locks | Locks
*PairlistApi* | [**blacklist_api_v1_blacklist_get**](docs/PairlistApi.md#blacklist_api_v1_blacklist_get) | **GET** /api/v1/blacklist | Blacklist
*PairlistApi* | [**blacklist_delete_api_v1_blacklist_delete**](docs/PairlistApi.md#blacklist_delete_api_v1_blacklist_delete) | **DELETE** /api/v1/blacklist | Blacklist Delete
*PairlistApi* | [**blacklist_post_api_v1_blacklist_post**](docs/PairlistApi.md#blacklist_post_api_v1_blacklist_post) | **POST** /api/v1/blacklist | Blacklist Post
*PairlistApi* | [**whitelist_api_v1_whitelist_get**](docs/PairlistApi.md#whitelist_api_v1_whitelist_get) | **GET** /api/v1/whitelist | Whitelist
*StrategyApi* | [**get_strategy_api_v1_strategy_strategy_get**](docs/StrategyApi.md#get_strategy_api_v1_strategy_strategy_get) | **GET** /api/v1/strategy/{strategy} | Get Strategy
*StrategyApi* | [**list_strategies_api_v1_strategies_get**](docs/StrategyApi.md#list_strategies_api_v1_strategies_get) | **GET** /api/v1/strategies | List Strategies
*TradingApi* | [**forcebuy_api_v1_forcebuy_post**](docs/TradingApi.md#forcebuy_api_v1_forcebuy_post) | **POST** /api/v1/forcebuy | Forcebuy
*TradingApi* | [**forcesell_api_v1_forcesell_post**](docs/TradingApi.md#forcesell_api_v1_forcesell_post) | **POST** /api/v1/forcesell | Forcesell
*TradingApi* | [**trade_api_v1_trade_tradeid_get**](docs/TradingApi.md#trade_api_v1_trade_tradeid_get) | **GET** /api/v1/trade/{tradeid} | Trade
*TradingApi* | [**trades_api_v1_trades_get**](docs/TradingApi.md#trades_api_v1_trades_get) | **GET** /api/v1/trades | Trades
*TradingApi* | [**trades_delete_api_v1_trades_tradeid_delete**](docs/TradingApi.md#trades_delete_api_v1_trades_tradeid_delete) | **DELETE** /api/v1/trades/{tradeid} | Trades Delete
*WebserverApi* | [**api_backtest_abort_api_v1_backtest_abort_get**](docs/WebserverApi.md#api_backtest_abort_api_v1_backtest_abort_get) | **GET** /api/v1/backtest/abort | Api Backtest Abort
*WebserverApi* | [**api_delete_backtest_api_v1_backtest_delete**](docs/WebserverApi.md#api_delete_backtest_api_v1_backtest_delete) | **DELETE** /api/v1/backtest | Api Delete Backtest
*WebserverApi* | [**api_get_backtest_api_v1_backtest_get**](docs/WebserverApi.md#api_get_backtest_api_v1_backtest_get) | **GET** /api/v1/backtest | Api Get Backtest
*WebserverApi* | [**api_start_backtest_api_v1_backtest_post**](docs/WebserverApi.md#api_start_backtest_api_v1_backtest_post) | **POST** /api/v1/backtest | Api Start Backtest


## Documentation For Models

 - [AccessAndRefreshToken](docs/AccessAndRefreshToken.md)
 - [AccessToken](docs/AccessToken.md)
 - [AvailablePairs](docs/AvailablePairs.md)
 - [BacktestRequest](docs/BacktestRequest.md)
 - [BacktestResponse](docs/BacktestResponse.md)
 - [Balance](docs/Balance.md)
 - [Balances](docs/Balances.md)
 - [BlacklistPayload](docs/BlacklistPayload.md)
 - [BlacklistResponse](docs/BlacklistResponse.md)
 - [Count](docs/Count.md)
 - [Daily](docs/Daily.md)
 - [DailyRecord](docs/DailyRecord.md)
 - [DeleteLockRequest](docs/DeleteLockRequest.md)
 - [DeleteTrade](docs/DeleteTrade.md)
 - [ForceBuyPayload](docs/ForceBuyPayload.md)
 - [ForceBuyResponse](docs/ForceBuyResponse.md)
 - [ForceSellPayload](docs/ForceSellPayload.md)
 - [HTTPValidationError](docs/HTTPValidationError.md)
 - [LockModel](docs/LockModel.md)
 - [Locks](docs/Locks.md)
 - [Logs](docs/Logs.md)
 - [OpenTradeSchema](docs/OpenTradeSchema.md)
 - [OrderTypeValues](docs/OrderTypeValues.md)
 - [OrderTypes](docs/OrderTypes.md)
 - [PairHistory](docs/PairHistory.md)
 - [PerformanceEntry](docs/PerformanceEntry.md)
 - [Ping](docs/Ping.md)
 - [PlotConfig](docs/PlotConfig.md)
 - [Profit](docs/Profit.md)
 - [ResultMsg](docs/ResultMsg.md)
 - [SellReason](docs/SellReason.md)
 - [ShowConfig](docs/ShowConfig.md)
 - [Stats](docs/Stats.md)
 - [StatusMsg](docs/StatusMsg.md)
 - [StrategyListResponse](docs/StrategyListResponse.md)
 - [StrategyResponse](docs/StrategyResponse.md)
 - [SysInfo](docs/SysInfo.md)
 - [TradeSchema](docs/TradeSchema.md)
 - [UnfilledTimeout](docs/UnfilledTimeout.md)
 - [ValidationError](docs/ValidationError.md)
 - [Version](docs/Version.md)
 - [WhitelistResponse](docs/WhitelistResponse.md)


## Documentation For Authorization


## HTTPBasic

- **Type**: HTTP basic authentication


## OAuth2PasswordBearer

- **Type**: OAuth
- **Flow**: password
- **Authorization URL**: 
- **Scopes**: N/A


## Author




## Notes for Large OpenAPI documents
If the OpenAPI document is large, imports in ftclient.apis and ftclient.models may fail with a
RecursionError indicating the maximum recursion limit has been exceeded. In that case, there are a couple of solutions:

Solution 1:
Use specific imports for apis and models like:
- `from ftclient.api.default_api import DefaultApi`
- `from ftclient.model.pet import Pet`

Solution 2:
Before importing the package, adjust the maximum recursion limit as shown below:
```
import sys
sys.setrecursionlimit(1500)
import ftclient
from ftclient.apis import *
from ftclient.models import *
```

