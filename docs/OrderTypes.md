# OrderTypes


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buy** | [**OrderTypeValues**](OrderTypeValues.md) |  | 
**sell** | [**OrderTypeValues**](OrderTypeValues.md) |  | 
**stoploss** | [**OrderTypeValues**](OrderTypeValues.md) |  | 
**stoploss_on_exchange** | **bool** |  | 
**emergencysell** | [**OrderTypeValues**](OrderTypeValues.md) |  | [optional] 
**forcesell** | [**OrderTypeValues**](OrderTypeValues.md) |  | [optional] 
**forcebuy** | [**OrderTypeValues**](OrderTypeValues.md) |  | [optional] 
**stoploss_on_exchange_interval** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


