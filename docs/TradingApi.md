# ftclient.TradingApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**forcebuy_api_v1_forcebuy_post**](TradingApi.md#forcebuy_api_v1_forcebuy_post) | **POST** /api/v1/forcebuy | Forcebuy
[**forcesell_api_v1_forcesell_post**](TradingApi.md#forcesell_api_v1_forcesell_post) | **POST** /api/v1/forcesell | Forcesell
[**trade_api_v1_trade_tradeid_get**](TradingApi.md#trade_api_v1_trade_tradeid_get) | **GET** /api/v1/trade/{tradeid} | Trade
[**trades_api_v1_trades_get**](TradingApi.md#trades_api_v1_trades_get) | **GET** /api/v1/trades | Trades
[**trades_delete_api_v1_trades_tradeid_delete**](TradingApi.md#trades_delete_api_v1_trades_tradeid_delete) | **DELETE** /api/v1/trades/{tradeid} | Trades Delete


# **forcebuy_api_v1_forcebuy_post**
> ForceBuyResponse forcebuy_api_v1_forcebuy_post(force_buy_payload)

Forcebuy

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import trading_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.force_buy_payload import ForceBuyPayload
from ftclient.model.force_buy_response import ForceBuyResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = trading_api.TradingApi(api_client)
    force_buy_payload = ForceBuyPayload(
        pair="pair_example",
        price=3.14,
        ordertype=OrderTypeValues("limit"),
    ) # ForceBuyPayload | 

    # example passing only required values which don't have defaults set
    try:
        # Forcebuy
        api_response = api_instance.forcebuy_api_v1_forcebuy_post(force_buy_payload)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling TradingApi->forcebuy_api_v1_forcebuy_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **force_buy_payload** | [**ForceBuyPayload**](ForceBuyPayload.md)|  |

### Return type

[**ForceBuyResponse**](ForceBuyResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **forcesell_api_v1_forcesell_post**
> ResultMsg forcesell_api_v1_forcesell_post(force_sell_payload)

Forcesell

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import trading_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.result_msg import ResultMsg
from ftclient.model.force_sell_payload import ForceSellPayload
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = trading_api.TradingApi(api_client)
    force_sell_payload = ForceSellPayload(
        tradeid="tradeid_example",
        ordertype=OrderTypeValues("limit"),
    ) # ForceSellPayload | 

    # example passing only required values which don't have defaults set
    try:
        # Forcesell
        api_response = api_instance.forcesell_api_v1_forcesell_post(force_sell_payload)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling TradingApi->forcesell_api_v1_forcesell_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **force_sell_payload** | [**ForceSellPayload**](ForceSellPayload.md)|  |

### Return type

[**ResultMsg**](ResultMsg.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **trade_api_v1_trade_tradeid_get**
> OpenTradeSchema trade_api_v1_trade_tradeid_get(tradeid)

Trade

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import trading_api
from ftclient.model.open_trade_schema import OpenTradeSchema
from ftclient.model.http_validation_error import HTTPValidationError
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = trading_api.TradingApi(api_client)
    tradeid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Trade
        api_response = api_instance.trade_api_v1_trade_tradeid_get(tradeid)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling TradingApi->trade_api_v1_trade_tradeid_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tradeid** | **int**|  |

### Return type

[**OpenTradeSchema**](OpenTradeSchema.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **trades_api_v1_trades_get**
> bool, date, datetime, dict, float, int, list, str, none_type trades_api_v1_trades_get()

Trades

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import trading_api
from ftclient.model.http_validation_error import HTTPValidationError
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = trading_api.TradingApi(api_client)
    limit = 500 # int |  (optional) if omitted the server will use the default value of 500
    offset = 0 # int |  (optional) if omitted the server will use the default value of 0

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Trades
        api_response = api_instance.trades_api_v1_trades_get(limit=limit, offset=offset)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling TradingApi->trades_api_v1_trades_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**|  | [optional] if omitted the server will use the default value of 500
 **offset** | **int**|  | [optional] if omitted the server will use the default value of 0

### Return type

**bool, date, datetime, dict, float, int, list, str, none_type**

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **trades_delete_api_v1_trades_tradeid_delete**
> DeleteTrade trades_delete_api_v1_trades_tradeid_delete(tradeid)

Trades Delete

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import trading_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.delete_trade import DeleteTrade
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = trading_api.TradingApi(api_client)
    tradeid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Trades Delete
        api_response = api_instance.trades_delete_api_v1_trades_tradeid_delete(tradeid)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling TradingApi->trades_delete_api_v1_trades_tradeid_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tradeid** | **int**|  |

### Return type

[**DeleteTrade**](DeleteTrade.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

