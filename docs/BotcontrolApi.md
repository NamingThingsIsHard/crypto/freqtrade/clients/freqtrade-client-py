# ftclient.BotcontrolApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**reload_config_api_v1_reload_config_post**](BotcontrolApi.md#reload_config_api_v1_reload_config_post) | **POST** /api/v1/reload_config | Reload Config
[**start_api_v1_start_post**](BotcontrolApi.md#start_api_v1_start_post) | **POST** /api/v1/start | Start
[**stop_api_v1_stop_post**](BotcontrolApi.md#stop_api_v1_stop_post) | **POST** /api/v1/stop | Stop
[**stop_buy_api_v1_stopbuy_post**](BotcontrolApi.md#stop_buy_api_v1_stopbuy_post) | **POST** /api/v1/stopbuy | Stop Buy


# **reload_config_api_v1_reload_config_post**
> StatusMsg reload_config_api_v1_reload_config_post()

Reload Config

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import botcontrol_api
from ftclient.model.status_msg import StatusMsg
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = botcontrol_api.BotcontrolApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Reload Config
        api_response = api_instance.reload_config_api_v1_reload_config_post()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling BotcontrolApi->reload_config_api_v1_reload_config_post: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**StatusMsg**](StatusMsg.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **start_api_v1_start_post**
> StatusMsg start_api_v1_start_post()

Start

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import botcontrol_api
from ftclient.model.status_msg import StatusMsg
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = botcontrol_api.BotcontrolApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Start
        api_response = api_instance.start_api_v1_start_post()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling BotcontrolApi->start_api_v1_start_post: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**StatusMsg**](StatusMsg.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stop_api_v1_stop_post**
> StatusMsg stop_api_v1_stop_post()

Stop

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import botcontrol_api
from ftclient.model.status_msg import StatusMsg
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = botcontrol_api.BotcontrolApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Stop
        api_response = api_instance.stop_api_v1_stop_post()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling BotcontrolApi->stop_api_v1_stop_post: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**StatusMsg**](StatusMsg.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stop_buy_api_v1_stopbuy_post**
> StatusMsg stop_buy_api_v1_stopbuy_post()

Stop Buy

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import botcontrol_api
from ftclient.model.status_msg import StatusMsg
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = botcontrol_api.BotcontrolApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Stop Buy
        api_response = api_instance.stop_buy_api_v1_stopbuy_post()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling BotcontrolApi->stop_buy_api_v1_stopbuy_post: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**StatusMsg**](StatusMsg.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

