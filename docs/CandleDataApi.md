# ftclient.CandleDataApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**list_available_pairs_api_v1_available_pairs_get**](CandleDataApi.md#list_available_pairs_api_v1_available_pairs_get) | **GET** /api/v1/available_pairs | List Available Pairs
[**pair_candles_api_v1_pair_candles_get**](CandleDataApi.md#pair_candles_api_v1_pair_candles_get) | **GET** /api/v1/pair_candles | Pair Candles
[**pair_history_api_v1_pair_history_get**](CandleDataApi.md#pair_history_api_v1_pair_history_get) | **GET** /api/v1/pair_history | Pair History
[**plot_config_api_v1_plot_config_get**](CandleDataApi.md#plot_config_api_v1_plot_config_get) | **GET** /api/v1/plot_config | Plot Config


# **list_available_pairs_api_v1_available_pairs_get**
> AvailablePairs list_available_pairs_api_v1_available_pairs_get()

List Available Pairs

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import candle_data_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.available_pairs import AvailablePairs
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = candle_data_api.CandleDataApi(api_client)
    timeframe = "timeframe_example" # str |  (optional)
    stake_currency = "stake_currency_example" # str |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # List Available Pairs
        api_response = api_instance.list_available_pairs_api_v1_available_pairs_get(timeframe=timeframe, stake_currency=stake_currency)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling CandleDataApi->list_available_pairs_api_v1_available_pairs_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeframe** | **str**|  | [optional]
 **stake_currency** | **str**|  | [optional]

### Return type

[**AvailablePairs**](AvailablePairs.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pair_candles_api_v1_pair_candles_get**
> PairHistory pair_candles_api_v1_pair_candles_get(pair, timeframe, limit)

Pair Candles

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import candle_data_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.pair_history import PairHistory
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = candle_data_api.CandleDataApi(api_client)
    pair = "pair_example" # str | 
    timeframe = "timeframe_example" # str | 
    limit = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Pair Candles
        api_response = api_instance.pair_candles_api_v1_pair_candles_get(pair, timeframe, limit)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling CandleDataApi->pair_candles_api_v1_pair_candles_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pair** | **str**|  |
 **timeframe** | **str**|  |
 **limit** | **int**|  |

### Return type

[**PairHistory**](PairHistory.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pair_history_api_v1_pair_history_get**
> PairHistory pair_history_api_v1_pair_history_get(pair, timeframe, timerange, strategy)

Pair History

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import candle_data_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.pair_history import PairHistory
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = candle_data_api.CandleDataApi(api_client)
    pair = "pair_example" # str | 
    timeframe = "timeframe_example" # str | 
    timerange = "timerange_example" # str | 
    strategy = "strategy_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Pair History
        api_response = api_instance.pair_history_api_v1_pair_history_get(pair, timeframe, timerange, strategy)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling CandleDataApi->pair_history_api_v1_pair_history_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pair** | **str**|  |
 **timeframe** | **str**|  |
 **timerange** | **str**|  |
 **strategy** | **str**|  |

### Return type

[**PairHistory**](PairHistory.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_config_api_v1_plot_config_get**
> PlotConfig plot_config_api_v1_plot_config_get()

Plot Config

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import candle_data_api
from ftclient.model.plot_config import PlotConfig
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = candle_data_api.CandleDataApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Plot Config
        api_response = api_instance.plot_config_api_v1_plot_config_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling CandleDataApi->plot_config_api_v1_plot_config_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**PlotConfig**](PlotConfig.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

