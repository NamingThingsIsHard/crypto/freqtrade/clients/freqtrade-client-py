# ftclient.WebserverApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_backtest_abort_api_v1_backtest_abort_get**](WebserverApi.md#api_backtest_abort_api_v1_backtest_abort_get) | **GET** /api/v1/backtest/abort | Api Backtest Abort
[**api_delete_backtest_api_v1_backtest_delete**](WebserverApi.md#api_delete_backtest_api_v1_backtest_delete) | **DELETE** /api/v1/backtest | Api Delete Backtest
[**api_get_backtest_api_v1_backtest_get**](WebserverApi.md#api_get_backtest_api_v1_backtest_get) | **GET** /api/v1/backtest | Api Get Backtest
[**api_start_backtest_api_v1_backtest_post**](WebserverApi.md#api_start_backtest_api_v1_backtest_post) | **POST** /api/v1/backtest | Api Start Backtest


# **api_backtest_abort_api_v1_backtest_abort_get**
> BacktestResponse api_backtest_abort_api_v1_backtest_abort_get()

Api Backtest Abort

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import webserver_api
from ftclient.model.backtest_response import BacktestResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = webserver_api.WebserverApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Api Backtest Abort
        api_response = api_instance.api_backtest_abort_api_v1_backtest_abort_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling WebserverApi->api_backtest_abort_api_v1_backtest_abort_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**BacktestResponse**](BacktestResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_delete_backtest_api_v1_backtest_delete**
> BacktestResponse api_delete_backtest_api_v1_backtest_delete()

Api Delete Backtest

Reset backtesting

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import webserver_api
from ftclient.model.backtest_response import BacktestResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = webserver_api.WebserverApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Api Delete Backtest
        api_response = api_instance.api_delete_backtest_api_v1_backtest_delete()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling WebserverApi->api_delete_backtest_api_v1_backtest_delete: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**BacktestResponse**](BacktestResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_get_backtest_api_v1_backtest_get**
> BacktestResponse api_get_backtest_api_v1_backtest_get()

Api Get Backtest

Get backtesting result. Returns Result after backtesting has been ran.

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import webserver_api
from ftclient.model.backtest_response import BacktestResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = webserver_api.WebserverApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Api Get Backtest
        api_response = api_instance.api_get_backtest_api_v1_backtest_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling WebserverApi->api_get_backtest_api_v1_backtest_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**BacktestResponse**](BacktestResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_start_backtest_api_v1_backtest_post**
> BacktestResponse api_start_backtest_api_v1_backtest_post(backtest_request)

Api Start Backtest

Start backtesting if not done so already

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import webserver_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.backtest_request import BacktestRequest
from ftclient.model.backtest_response import BacktestResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = webserver_api.WebserverApi(api_client)
    backtest_request = BacktestRequest(
        strategy="strategy_example",
        timeframe="timeframe_example",
        timeframe_detail="timeframe_detail_example",
        timerange="timerange_example",
        max_open_trades=1,
        stake_amount=None,
        enable_protections=True,
        dry_run_wallet=3.14,
    ) # BacktestRequest | 

    # example passing only required values which don't have defaults set
    try:
        # Api Start Backtest
        api_response = api_instance.api_start_backtest_api_v1_backtest_post(backtest_request)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling WebserverApi->api_start_backtest_api_v1_backtest_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **backtest_request** | [**BacktestRequest**](BacktestRequest.md)|  |

### Return type

[**BacktestResponse**](BacktestResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

