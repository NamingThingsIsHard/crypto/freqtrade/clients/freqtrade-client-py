# ftclient.LocksApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_lock_api_v1_locks_lockid_delete**](LocksApi.md#delete_lock_api_v1_locks_lockid_delete) | **DELETE** /api/v1/locks/{lockid} | Delete Lock
[**delete_lock_pair_api_v1_locks_delete_post**](LocksApi.md#delete_lock_pair_api_v1_locks_delete_post) | **POST** /api/v1/locks/delete | Delete Lock Pair
[**locks_api_v1_locks_get**](LocksApi.md#locks_api_v1_locks_get) | **GET** /api/v1/locks | Locks


# **delete_lock_api_v1_locks_lockid_delete**
> Locks delete_lock_api_v1_locks_lockid_delete(lockid)

Delete Lock

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import locks_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.locks import Locks
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = locks_api.LocksApi(api_client)
    lockid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Delete Lock
        api_response = api_instance.delete_lock_api_v1_locks_lockid_delete(lockid)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling LocksApi->delete_lock_api_v1_locks_lockid_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lockid** | **int**|  |

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_lock_pair_api_v1_locks_delete_post**
> Locks delete_lock_pair_api_v1_locks_delete_post(delete_lock_request)

Delete Lock Pair

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import locks_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.delete_lock_request import DeleteLockRequest
from ftclient.model.locks import Locks
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = locks_api.LocksApi(api_client)
    delete_lock_request = DeleteLockRequest(
        pair="pair_example",
        lockid=1,
    ) # DeleteLockRequest | 

    # example passing only required values which don't have defaults set
    try:
        # Delete Lock Pair
        api_response = api_instance.delete_lock_pair_api_v1_locks_delete_post(delete_lock_request)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling LocksApi->delete_lock_pair_api_v1_locks_delete_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_lock_request** | [**DeleteLockRequest**](DeleteLockRequest.md)|  |

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **locks_api_v1_locks_get**
> Locks locks_api_v1_locks_get()

Locks

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import locks_api
from ftclient.model.locks import Locks
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = locks_api.LocksApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Locks
        api_response = api_instance.locks_api_v1_locks_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling LocksApi->locks_api_v1_locks_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

