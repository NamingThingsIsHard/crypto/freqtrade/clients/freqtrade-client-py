# Balances


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currencies** | [**[Balance]**](Balance.md) |  | 
**total** | **float** |  | 
**symbol** | **str** |  | 
**value** | **float** |  | 
**stake** | **str** |  | 
**note** | **str** |  | 
**starting_capital** | **float** |  | 
**starting_capital_ratio** | **float** |  | 
**starting_capital_pct** | **float** |  | 
**starting_capital_fiat** | **float** |  | 
**starting_capital_fiat_ratio** | **float** |  | 
**starting_capital_fiat_pct** | **float** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


