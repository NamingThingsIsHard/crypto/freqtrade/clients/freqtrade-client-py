# ftclient.PairlistApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blacklist_api_v1_blacklist_get**](PairlistApi.md#blacklist_api_v1_blacklist_get) | **GET** /api/v1/blacklist | Blacklist
[**blacklist_delete_api_v1_blacklist_delete**](PairlistApi.md#blacklist_delete_api_v1_blacklist_delete) | **DELETE** /api/v1/blacklist | Blacklist Delete
[**blacklist_post_api_v1_blacklist_post**](PairlistApi.md#blacklist_post_api_v1_blacklist_post) | **POST** /api/v1/blacklist | Blacklist Post
[**whitelist_api_v1_whitelist_get**](PairlistApi.md#whitelist_api_v1_whitelist_get) | **GET** /api/v1/whitelist | Whitelist


# **blacklist_api_v1_blacklist_get**
> BlacklistResponse blacklist_api_v1_blacklist_get()

Blacklist

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import pairlist_api
from ftclient.model.blacklist_response import BlacklistResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pairlist_api.PairlistApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Blacklist
        api_response = api_instance.blacklist_api_v1_blacklist_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling PairlistApi->blacklist_api_v1_blacklist_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blacklist_delete_api_v1_blacklist_delete**
> BlacklistResponse blacklist_delete_api_v1_blacklist_delete()

Blacklist Delete

Provide a list of pairs to delete from the blacklist

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import pairlist_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.blacklist_response import BlacklistResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pairlist_api.PairlistApi(api_client)
    pairs_to_delete = [] # [str] |  (optional) if omitted the server will use the default value of []

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Blacklist Delete
        api_response = api_instance.blacklist_delete_api_v1_blacklist_delete(pairs_to_delete=pairs_to_delete)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling PairlistApi->blacklist_delete_api_v1_blacklist_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pairs_to_delete** | **[str]**|  | [optional] if omitted the server will use the default value of []

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blacklist_post_api_v1_blacklist_post**
> BlacklistResponse blacklist_post_api_v1_blacklist_post(blacklist_payload)

Blacklist Post

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import pairlist_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.blacklist_payload import BlacklistPayload
from ftclient.model.blacklist_response import BlacklistResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pairlist_api.PairlistApi(api_client)
    blacklist_payload = BlacklistPayload(
        blacklist=[
            "blacklist_example",
        ],
    ) # BlacklistPayload | 

    # example passing only required values which don't have defaults set
    try:
        # Blacklist Post
        api_response = api_instance.blacklist_post_api_v1_blacklist_post(blacklist_payload)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling PairlistApi->blacklist_post_api_v1_blacklist_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blacklist_payload** | [**BlacklistPayload**](BlacklistPayload.md)|  |

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **whitelist_api_v1_whitelist_get**
> WhitelistResponse whitelist_api_v1_whitelist_get()

Whitelist

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import pairlist_api
from ftclient.model.whitelist_response import WhitelistResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = pairlist_api.PairlistApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Whitelist
        api_response = api_instance.whitelist_api_v1_whitelist_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling PairlistApi->whitelist_api_v1_whitelist_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**WhitelistResponse**](WhitelistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

