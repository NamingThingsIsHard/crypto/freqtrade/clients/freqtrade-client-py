# ftclient.InfoApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**balance_api_v1_balance_get**](InfoApi.md#balance_api_v1_balance_get) | **GET** /api/v1/balance | Balance
[**blacklist_api_v1_blacklist_get**](InfoApi.md#blacklist_api_v1_blacklist_get) | **GET** /api/v1/blacklist | Blacklist
[**blacklist_delete_api_v1_blacklist_delete**](InfoApi.md#blacklist_delete_api_v1_blacklist_delete) | **DELETE** /api/v1/blacklist | Blacklist Delete
[**blacklist_post_api_v1_blacklist_post**](InfoApi.md#blacklist_post_api_v1_blacklist_post) | **POST** /api/v1/blacklist | Blacklist Post
[**count_api_v1_count_get**](InfoApi.md#count_api_v1_count_get) | **GET** /api/v1/count | Count
[**daily_api_v1_daily_get**](InfoApi.md#daily_api_v1_daily_get) | **GET** /api/v1/daily | Daily
[**delete_lock_api_v1_locks_lockid_delete**](InfoApi.md#delete_lock_api_v1_locks_lockid_delete) | **DELETE** /api/v1/locks/{lockid} | Delete Lock
[**delete_lock_pair_api_v1_locks_delete_post**](InfoApi.md#delete_lock_pair_api_v1_locks_delete_post) | **POST** /api/v1/locks/delete | Delete Lock Pair
[**edge_api_v1_edge_get**](InfoApi.md#edge_api_v1_edge_get) | **GET** /api/v1/edge | Edge
[**locks_api_v1_locks_get**](InfoApi.md#locks_api_v1_locks_get) | **GET** /api/v1/locks | Locks
[**logs_api_v1_logs_get**](InfoApi.md#logs_api_v1_logs_get) | **GET** /api/v1/logs | Logs
[**performance_api_v1_performance_get**](InfoApi.md#performance_api_v1_performance_get) | **GET** /api/v1/performance | Performance
[**profit_api_v1_profit_get**](InfoApi.md#profit_api_v1_profit_get) | **GET** /api/v1/profit | Profit
[**show_config_api_v1_show_config_get**](InfoApi.md#show_config_api_v1_show_config_get) | **GET** /api/v1/show_config | Show Config
[**stats_api_v1_stats_get**](InfoApi.md#stats_api_v1_stats_get) | **GET** /api/v1/stats | Stats
[**status_api_v1_status_get**](InfoApi.md#status_api_v1_status_get) | **GET** /api/v1/status | Status
[**sysinfo_api_v1_sysinfo_get**](InfoApi.md#sysinfo_api_v1_sysinfo_get) | **GET** /api/v1/sysinfo | Sysinfo
[**trade_api_v1_trade_tradeid_get**](InfoApi.md#trade_api_v1_trade_tradeid_get) | **GET** /api/v1/trade/{tradeid} | Trade
[**trades_api_v1_trades_get**](InfoApi.md#trades_api_v1_trades_get) | **GET** /api/v1/trades | Trades
[**trades_delete_api_v1_trades_tradeid_delete**](InfoApi.md#trades_delete_api_v1_trades_tradeid_delete) | **DELETE** /api/v1/trades/{tradeid} | Trades Delete
[**version_api_v1_version_get**](InfoApi.md#version_api_v1_version_get) | **GET** /api/v1/version | Version
[**whitelist_api_v1_whitelist_get**](InfoApi.md#whitelist_api_v1_whitelist_get) | **GET** /api/v1/whitelist | Whitelist


# **balance_api_v1_balance_get**
> Balances balance_api_v1_balance_get()

Balance

Account Balances

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.balances import Balances
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Balance
        api_response = api_instance.balance_api_v1_balance_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->balance_api_v1_balance_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Balances**](Balances.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blacklist_api_v1_blacklist_get**
> BlacklistResponse blacklist_api_v1_blacklist_get()

Blacklist

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.blacklist_response import BlacklistResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Blacklist
        api_response = api_instance.blacklist_api_v1_blacklist_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->blacklist_api_v1_blacklist_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blacklist_delete_api_v1_blacklist_delete**
> BlacklistResponse blacklist_delete_api_v1_blacklist_delete()

Blacklist Delete

Provide a list of pairs to delete from the blacklist

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.blacklist_response import BlacklistResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)
    pairs_to_delete = [] # [str] |  (optional) if omitted the server will use the default value of []

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Blacklist Delete
        api_response = api_instance.blacklist_delete_api_v1_blacklist_delete(pairs_to_delete=pairs_to_delete)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->blacklist_delete_api_v1_blacklist_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pairs_to_delete** | **[str]**|  | [optional] if omitted the server will use the default value of []

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blacklist_post_api_v1_blacklist_post**
> BlacklistResponse blacklist_post_api_v1_blacklist_post(blacklist_payload)

Blacklist Post

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.blacklist_payload import BlacklistPayload
from ftclient.model.blacklist_response import BlacklistResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)
    blacklist_payload = BlacklistPayload(
        blacklist=[
            "blacklist_example",
        ],
    ) # BlacklistPayload | 

    # example passing only required values which don't have defaults set
    try:
        # Blacklist Post
        api_response = api_instance.blacklist_post_api_v1_blacklist_post(blacklist_payload)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->blacklist_post_api_v1_blacklist_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blacklist_payload** | [**BlacklistPayload**](BlacklistPayload.md)|  |

### Return type

[**BlacklistResponse**](BlacklistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **count_api_v1_count_get**
> Count count_api_v1_count_get()

Count

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.count import Count
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Count
        api_response = api_instance.count_api_v1_count_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->count_api_v1_count_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Count**](Count.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **daily_api_v1_daily_get**
> Daily daily_api_v1_daily_get()

Daily

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.daily import Daily
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)
    timescale = 7 # int |  (optional) if omitted the server will use the default value of 7

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Daily
        api_response = api_instance.daily_api_v1_daily_get(timescale=timescale)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->daily_api_v1_daily_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timescale** | **int**|  | [optional] if omitted the server will use the default value of 7

### Return type

[**Daily**](Daily.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_lock_api_v1_locks_lockid_delete**
> Locks delete_lock_api_v1_locks_lockid_delete(lockid)

Delete Lock

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.locks import Locks
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)
    lockid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Delete Lock
        api_response = api_instance.delete_lock_api_v1_locks_lockid_delete(lockid)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->delete_lock_api_v1_locks_lockid_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lockid** | **int**|  |

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_lock_pair_api_v1_locks_delete_post**
> Locks delete_lock_pair_api_v1_locks_delete_post(delete_lock_request)

Delete Lock Pair

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.delete_lock_request import DeleteLockRequest
from ftclient.model.locks import Locks
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)
    delete_lock_request = DeleteLockRequest(
        pair="pair_example",
        lockid=1,
    ) # DeleteLockRequest | 

    # example passing only required values which don't have defaults set
    try:
        # Delete Lock Pair
        api_response = api_instance.delete_lock_pair_api_v1_locks_delete_post(delete_lock_request)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->delete_lock_pair_api_v1_locks_delete_post: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_lock_request** | [**DeleteLockRequest**](DeleteLockRequest.md)|  |

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **edge_api_v1_edge_get**
> bool, date, datetime, dict, float, int, list, str, none_type edge_api_v1_edge_get()

Edge

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Edge
        api_response = api_instance.edge_api_v1_edge_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->edge_api_v1_edge_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

**bool, date, datetime, dict, float, int, list, str, none_type**

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **locks_api_v1_locks_get**
> Locks locks_api_v1_locks_get()

Locks

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.locks import Locks
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Locks
        api_response = api_instance.locks_api_v1_locks_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->locks_api_v1_locks_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Locks**](Locks.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **logs_api_v1_logs_get**
> Logs logs_api_v1_logs_get()

Logs

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.logs import Logs
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)
    limit = 1 # int |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Logs
        api_response = api_instance.logs_api_v1_logs_get(limit=limit)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->logs_api_v1_logs_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**|  | [optional]

### Return type

[**Logs**](Logs.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **performance_api_v1_performance_get**
> [PerformanceEntry] performance_api_v1_performance_get()

Performance

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.performance_entry import PerformanceEntry
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Performance
        api_response = api_instance.performance_api_v1_performance_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->performance_api_v1_performance_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[PerformanceEntry]**](PerformanceEntry.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **profit_api_v1_profit_get**
> Profit profit_api_v1_profit_get()

Profit

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.profit import Profit
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Profit
        api_response = api_instance.profit_api_v1_profit_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->profit_api_v1_profit_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Profit**](Profit.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **show_config_api_v1_show_config_get**
> ShowConfig show_config_api_v1_show_config_get()

Show Config

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.show_config import ShowConfig
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Show Config
        api_response = api_instance.show_config_api_v1_show_config_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->show_config_api_v1_show_config_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**ShowConfig**](ShowConfig.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stats_api_v1_stats_get**
> Stats stats_api_v1_stats_get()

Stats

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.stats import Stats
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Stats
        api_response = api_instance.stats_api_v1_stats_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->stats_api_v1_stats_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Stats**](Stats.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **status_api_v1_status_get**
> [OpenTradeSchema] status_api_v1_status_get()

Status

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.open_trade_schema import OpenTradeSchema
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Status
        api_response = api_instance.status_api_v1_status_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->status_api_v1_status_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**[OpenTradeSchema]**](OpenTradeSchema.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sysinfo_api_v1_sysinfo_get**
> SysInfo sysinfo_api_v1_sysinfo_get()

Sysinfo

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.sys_info import SysInfo
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Sysinfo
        api_response = api_instance.sysinfo_api_v1_sysinfo_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->sysinfo_api_v1_sysinfo_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**SysInfo**](SysInfo.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **trade_api_v1_trade_tradeid_get**
> OpenTradeSchema trade_api_v1_trade_tradeid_get(tradeid)

Trade

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.open_trade_schema import OpenTradeSchema
from ftclient.model.http_validation_error import HTTPValidationError
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)
    tradeid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Trade
        api_response = api_instance.trade_api_v1_trade_tradeid_get(tradeid)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->trade_api_v1_trade_tradeid_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tradeid** | **int**|  |

### Return type

[**OpenTradeSchema**](OpenTradeSchema.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **trades_api_v1_trades_get**
> bool, date, datetime, dict, float, int, list, str, none_type trades_api_v1_trades_get()

Trades

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.http_validation_error import HTTPValidationError
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)
    limit = 500 # int |  (optional) if omitted the server will use the default value of 500
    offset = 0 # int |  (optional) if omitted the server will use the default value of 0

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Trades
        api_response = api_instance.trades_api_v1_trades_get(limit=limit, offset=offset)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->trades_api_v1_trades_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**|  | [optional] if omitted the server will use the default value of 500
 **offset** | **int**|  | [optional] if omitted the server will use the default value of 0

### Return type

**bool, date, datetime, dict, float, int, list, str, none_type**

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **trades_delete_api_v1_trades_tradeid_delete**
> DeleteTrade trades_delete_api_v1_trades_tradeid_delete(tradeid)

Trades Delete

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.http_validation_error import HTTPValidationError
from ftclient.model.delete_trade import DeleteTrade
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)
    tradeid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Trades Delete
        api_response = api_instance.trades_delete_api_v1_trades_tradeid_delete(tradeid)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->trades_delete_api_v1_trades_tradeid_delete: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tradeid** | **int**|  |

### Return type

[**DeleteTrade**](DeleteTrade.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **version_api_v1_version_get**
> Version version_api_v1_version_get()

Version

Bot Version info

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.version import Version
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Version
        api_response = api_instance.version_api_v1_version_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->version_api_v1_version_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Version**](Version.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **whitelist_api_v1_whitelist_get**
> WhitelistResponse whitelist_api_v1_whitelist_get()

Whitelist

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import info_api
from ftclient.model.whitelist_response import WhitelistResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = info_api.InfoApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Whitelist
        api_response = api_instance.whitelist_api_v1_whitelist_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling InfoApi->whitelist_api_v1_whitelist_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**WhitelistResponse**](WhitelistResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

