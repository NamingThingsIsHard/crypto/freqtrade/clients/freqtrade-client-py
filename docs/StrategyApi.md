# ftclient.StrategyApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_strategy_api_v1_strategy_strategy_get**](StrategyApi.md#get_strategy_api_v1_strategy_strategy_get) | **GET** /api/v1/strategy/{strategy} | Get Strategy
[**list_strategies_api_v1_strategies_get**](StrategyApi.md#list_strategies_api_v1_strategies_get) | **GET** /api/v1/strategies | List Strategies


# **get_strategy_api_v1_strategy_strategy_get**
> StrategyResponse get_strategy_api_v1_strategy_strategy_get(strategy)

Get Strategy

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import strategy_api
from ftclient.model.strategy_response import StrategyResponse
from ftclient.model.http_validation_error import HTTPValidationError
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = strategy_api.StrategyApi(api_client)
    strategy = "strategy_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        # Get Strategy
        api_response = api_instance.get_strategy_api_v1_strategy_strategy_get(strategy)
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling StrategyApi->get_strategy_api_v1_strategy_strategy_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **strategy** | **str**|  |

### Return type

[**StrategyResponse**](StrategyResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_strategies_api_v1_strategies_get**
> StrategyListResponse list_strategies_api_v1_strategies_get()

List Strategies

### Example

* Basic Authentication (HTTPBasic):
* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import strategy_api
from ftclient.model.strategy_list_response import StrategyListResponse
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = strategy_api.StrategyApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # List Strategies
        api_response = api_instance.list_strategies_api_v1_strategies_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling StrategyApi->list_strategies_api_v1_strategies_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**StrategyListResponse**](StrategyListResponse.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic), [OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

