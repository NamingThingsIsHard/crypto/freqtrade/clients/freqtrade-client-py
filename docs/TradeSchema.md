# TradeSchema


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trade_id** | **int** |  | 
**pair** | **str** |  | 
**is_open** | **bool** |  | 
**exchange** | **str** |  | 
**amount** | **float** |  | 
**amount_requested** | **float** |  | 
**stake_amount** | **float** |  | 
**strategy** | **str** |  | 
**timeframe** | **int** |  | 
**open_date** | **str** |  | 
**open_timestamp** | **int** |  | 
**open_rate** | **float** |  | 
**open_trade_value** | **float** |  | 
**buy_tag** | **str** |  | [optional] 
**fee_open** | **float** |  | [optional] 
**fee_open_cost** | **float** |  | [optional] 
**fee_open_currency** | **str** |  | [optional] 
**fee_close** | **float** |  | [optional] 
**fee_close_cost** | **float** |  | [optional] 
**fee_close_currency** | **str** |  | [optional] 
**open_rate_requested** | **float** |  | [optional] 
**close_date** | **str** |  | [optional] 
**close_timestamp** | **int** |  | [optional] 
**close_rate** | **float** |  | [optional] 
**close_rate_requested** | **float** |  | [optional] 
**close_profit** | **float** |  | [optional] 
**close_profit_pct** | **float** |  | [optional] 
**close_profit_abs** | **float** |  | [optional] 
**profit_ratio** | **float** |  | [optional] 
**profit_pct** | **float** |  | [optional] 
**profit_abs** | **float** |  | [optional] 
**profit_fiat** | **float** |  | [optional] 
**sell_reason** | **str** |  | [optional] 
**sell_order_status** | **str** |  | [optional] 
**stop_loss_abs** | **float** |  | [optional] 
**stop_loss_ratio** | **float** |  | [optional] 
**stop_loss_pct** | **float** |  | [optional] 
**stoploss_order_id** | **str** |  | [optional] 
**stoploss_last_update** | **str** |  | [optional] 
**stoploss_last_update_timestamp** | **int** |  | [optional] 
**initial_stop_loss_abs** | **float** |  | [optional] 
**initial_stop_loss_ratio** | **float** |  | [optional] 
**initial_stop_loss_pct** | **float** |  | [optional] 
**min_rate** | **float** |  | [optional] 
**max_rate** | **float** |  | [optional] 
**open_order_id** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


