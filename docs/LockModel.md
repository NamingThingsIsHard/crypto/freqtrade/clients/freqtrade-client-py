# LockModel


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**active** | **bool** |  | 
**lock_end_time** | **str** |  | 
**lock_end_timestamp** | **int** |  | 
**lock_time** | **str** |  | 
**lock_timestamp** | **int** |  | 
**pair** | **str** |  | 
**reason** | **str** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


