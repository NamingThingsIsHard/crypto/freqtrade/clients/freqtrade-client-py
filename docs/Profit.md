# Profit


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profit_closed_coin** | **float** |  | 
**profit_closed_percent_mean** | **float** |  | 
**profit_closed_ratio_mean** | **float** |  | 
**profit_closed_percent_sum** | **float** |  | 
**profit_closed_ratio_sum** | **float** |  | 
**profit_closed_percent** | **float** |  | 
**profit_closed_ratio** | **float** |  | 
**profit_closed_fiat** | **float** |  | 
**profit_all_coin** | **float** |  | 
**profit_all_percent_mean** | **float** |  | 
**profit_all_ratio_mean** | **float** |  | 
**profit_all_percent_sum** | **float** |  | 
**profit_all_ratio_sum** | **float** |  | 
**profit_all_percent** | **float** |  | 
**profit_all_ratio** | **float** |  | 
**profit_all_fiat** | **float** |  | 
**trade_count** | **int** |  | 
**closed_trade_count** | **int** |  | 
**first_trade_date** | **str** |  | 
**first_trade_timestamp** | **int** |  | 
**latest_trade_date** | **str** |  | 
**latest_trade_timestamp** | **int** |  | 
**avg_duration** | **str** |  | 
**best_pair** | **str** |  | 
**best_rate** | **float** |  | 
**best_pair_profit_ratio** | **float** |  | 
**winning_trades** | **int** |  | 
**losing_trades** | **int** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


