# PairHistory


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**strategy** | **str** |  | 
**pair** | **str** |  | 
**timeframe** | **str** |  | 
**timeframe_ms** | **int** |  | 
**columns** | **[str]** |  | 
**data** | **[bool, date, datetime, dict, float, int, list, str, none_type]** |  | 
**length** | **int** |  | 
**buy_signals** | **int** |  | 
**sell_signals** | **int** |  | 
**last_analyzed** | **datetime** |  | 
**last_analyzed_ts** | **int** |  | 
**data_start_ts** | **int** |  | 
**data_start** | **str** |  | 
**data_stop** | **str** |  | 
**data_stop_ts** | **int** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


