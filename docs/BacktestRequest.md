# BacktestRequest


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**strategy** | **str** |  | 
**enable_protections** | **bool** |  | 
**timeframe** | **str** |  | [optional] 
**timeframe_detail** | **str** |  | [optional] 
**timerange** | **str** |  | [optional] 
**max_open_trades** | **int** |  | [optional] 
**stake_amount** | **bool, date, datetime, dict, float, int, list, str, none_type** |  | [optional] 
**dry_run_wallet** | **float** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


