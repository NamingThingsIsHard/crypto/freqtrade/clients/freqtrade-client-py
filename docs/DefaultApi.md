# ftclient.DefaultApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ping_api_v1_ping_get**](DefaultApi.md#ping_api_v1_ping_get) | **GET** /api/v1/ping | Ping


# **ping_api_v1_ping_get**
> Ping ping_api_v1_ping_get()

Ping

simple ping

### Example


```python
import time
import ftclient
from ftclient.api import default_api
from ftclient.model.ping import Ping
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)


# Enter a context with an instance of the API client
with ftclient.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = default_api.DefaultApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Ping
        api_response = api_instance.ping_api_v1_ping_get()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling DefaultApi->ping_api_v1_ping_get: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**Ping**](Ping.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

