# ftclient.AuthApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**token_login_api_v1_token_login_post**](AuthApi.md#token_login_api_v1_token_login_post) | **POST** /api/v1/token/login | Token Login
[**token_refresh_api_v1_token_refresh_post**](AuthApi.md#token_refresh_api_v1_token_refresh_post) | **POST** /api/v1/token/refresh | Token Refresh


# **token_login_api_v1_token_login_post**
> AccessAndRefreshToken token_login_api_v1_token_login_post()

Token Login

### Example

* Basic Authentication (HTTPBasic):

```python
import time
import ftclient
from ftclient.api import auth_api
from ftclient.model.access_and_refresh_token import AccessAndRefreshToken
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = ftclient.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Token Login
        api_response = api_instance.token_login_api_v1_token_login_post()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling AuthApi->token_login_api_v1_token_login_post: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**AccessAndRefreshToken**](AccessAndRefreshToken.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **token_refresh_api_v1_token_refresh_post**
> AccessToken token_refresh_api_v1_token_refresh_post()

Token Refresh

### Example

* OAuth Authentication (OAuth2PasswordBearer):

```python
import time
import ftclient
from ftclient.api import auth_api
from ftclient.model.access_token import AccessToken
from pprint import pprint
# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = ftclient.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2PasswordBearer
configuration = ftclient.Configuration(
    host = "http://localhost"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with ftclient.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Token Refresh
        api_response = api_instance.token_refresh_api_v1_token_refresh_post()
        pprint(api_response)
    except ftclient.ApiException as e:
        print("Exception when calling AuthApi->token_refresh_api_v1_token_refresh_post: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**AccessToken**](AccessToken.md)

### Authorization

[OAuth2PasswordBearer](../README.md#OAuth2PasswordBearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

