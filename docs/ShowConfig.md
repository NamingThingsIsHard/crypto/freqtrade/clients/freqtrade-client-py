# ShowConfig


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **str** |  | 
**api_version** | **float** |  | 
**dry_run** | **bool** |  | 
**stake_currency** | **str** |  | 
**stake_amount** | **bool, date, datetime, dict, float, int, list, str, none_type** |  | 
**stake_currency_decimals** | **int** |  | 
**max_open_trades** | **int** |  | 
**minimal_roi** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | 
**unfilledtimeout** | [**UnfilledTimeout**](UnfilledTimeout.md) |  | 
**timeframe_ms** | **int** |  | 
**timeframe_min** | **int** |  | 
**exchange** | **str** |  | 
**forcebuy_enabled** | **bool** |  | 
**ask_strategy** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | 
**bid_strategy** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** |  | 
**bot_name** | **str** |  | 
**state** | **str** |  | 
**runmode** | **str** |  | 
**strategy_version** | **str** |  | [optional] 
**available_capital** | **float** |  | [optional] 
**stoploss** | **float** |  | [optional] 
**trailing_stop** | **bool** |  | [optional] 
**trailing_stop_positive** | **float** |  | [optional] 
**trailing_stop_positive_offset** | **float** |  | [optional] 
**trailing_only_offset_is_reached** | **bool** |  | [optional] 
**order_types** | [**OrderTypes**](OrderTypes.md) |  | [optional] 
**use_custom_stoploss** | **bool** |  | [optional] 
**timeframe** | **str** |  | [optional] 
**strategy** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


